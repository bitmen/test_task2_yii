<?php

/**
 * 	Команда автоматического обновления вакансий, практик и объявлений жилья
 *
 * @author webdevsega
 */
class AutoUpdateAPCommand extends CConsoleCommand {

    ///https://github.com/aarondfrancis/yii-CronCommand/blob/master/CronCommand.php
    private function getMicrotime() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        return $time;
    }

    private function totalTime($start) {
        $finish = $this->getMicrotime();
        return round(($finish - $start), 4);
    }

    public function actionIndex() {

        $start = $this->getMicrotime();

        $this->autoUpdateAdCompany();

        // if, God forbid, this script ever run longer than 9 minutes, abort the loop.
        // If you don't heroku will kill the script after 10 (cron runs every 10 minutes).
        // I'd rather end it myself cleanly and let the next iteration pick up whatever is
        // left to be processed.
        if ($this->totalTime($start) >= (1 * 60))
            break;
    }

    public function autoUpdateAdCompany() {

        echo "Processing Job 3\r\n";

        $companies = Companyapartment::model()->findAll('auto_update > 0 and date_auto_update >= :now', array(':now' => date('Y-m-d H:i:s', time())));

        foreach ($companies as $company) {
            if ($company->last_auto_update != '0000-00-00 00:00:00') {
                $timeAfterUpdate = round((time() - strtotime($company->last_auto_update)) / 3600);
                if ($timeAfterUpdate < (int) $company->auto_update)
                    continue;
            }

            $criteria = new CDbCriteria;
            $criteria->condition = "f_company_id = {$company->companyapartment_id}";
            $criteria->limit = 10;
            $criteria->order = 'update_time DESC';

            $objects = Apartment::model()->findAll($criteria);

            foreach ($objects as $index => $object) {
                $updateParams = array();
                $updateParams['update_time'] = date('Y-m-d H:i:s', time() - 60 * $index);

                $object->saveAttributes($updateParams);
            }

            $company->saveAttributes(array('last_auto_update' => date('Y-m-d H:i:s', time())));
        }
    }

    public function testJob($params) {
    // this job doesn't do anything, just returns success.
        return array(
            'saveAttributes' => true,
            'execution_result' => 'Job succeeded'
        );
    }

}

?>
