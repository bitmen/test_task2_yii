<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->user_id,
);
?>

<h1>View User #<?php echo $model->user_id; ?></h1>
<hr />
<?php 
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		array('label'=>'Create', 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                array('label'=>'List', 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'), 'linkOptions'=>array()),
                array('label'=>'Update', 'icon'=>'icon-edit', 'url'=>Yii::app()->controller->createUrl('update',array('id'=>$model->user_id)), 'linkOptions'=>array()),
		//array('label'=>'Search', 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		array('label'=>'Print', 'icon'=>'icon-print', 'url'=>'javascript:void(0);return false', 'linkOptions'=>array('onclick'=>'printDiv();return false;')),

)));
$this->endWidget();
?>
<div class='printableArea'>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'email',
		'password',
		'f_district_id',
		'status',
		'user_key',
		'register_date',
		'confirm_date',
		'role',
		'auth_id',
		'service',
		'fio',
		'employment',
		'phone',
		'f_company_id',
		'last_visit_date',
		'login',
		'sex',
		'first_name',
		'last_name',
		'father_name',
		'contact_email',
		'skype',
		'info',
		'education',
		'work',
		'birthday',
		'has_logo',
		'member_date',
		'f_repetitor_subject_id',
		'update_time',
		'utime',
		'f_city_id',
	),
)); ?>
</div>
<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>
