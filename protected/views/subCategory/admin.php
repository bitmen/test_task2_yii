<?php
$this->breadcrumbs=array(
	'Sub Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SubCategory','url'=>array('index')),
	array('label'=>'Create SubCategory','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sub-category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sub Categories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'sub-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'sub_category_id',
		'f_category_id',
		'title',
		'list_pos',
		'translit',
		'_old_id',
		/*
		'seo_title',
		'seo_description',
		'seo_keywords',
		'seo_texttop',
		'seo_textbottom',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
