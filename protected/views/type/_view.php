<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->type_id),array('view','id'=>$data->type_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class_name')); ?>:</b>
	<?php echo CHtml::encode($data->class_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_title_pattern')); ?>:</b>
	<?php echo CHtml::encode($data->seo_title_pattern); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_title')); ?>:</b>
	<?php echo CHtml::encode($data->seo_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_description')); ?>:</b>
	<?php echo CHtml::encode($data->seo_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_keywords')); ?>:</b>
	<?php echo CHtml::encode($data->seo_keywords); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_texttop')); ?>:</b>
	<?php echo CHtml::encode($data->seo_texttop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seo_textbottom')); ?>:</b>
	<?php echo CHtml::encode($data->seo_textbottom); ?>
	<br />

	*/ ?>

</div>