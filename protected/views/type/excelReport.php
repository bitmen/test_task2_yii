<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      type_id		</th>
 		<th width="80px">
		      title		</th>
 		<th width="80px">
		      class_name		</th>
 		<th width="80px">
		      seo_title_pattern		</th>
 		<th width="80px">
		      seo_title		</th>
 		<th width="80px">
		      seo_description		</th>
 		<th width="80px">
		      seo_keywords		</th>
 		<th width="80px">
		      seo_texttop		</th>
 		<th width="80px">
		      seo_textbottom		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->type_id; ?>
		</td>
       		<td>
			<?php echo $row->title; ?>
		</td>
       		<td>
			<?php echo $row->class_name; ?>
		</td>
       		<td>
			<?php echo $row->seo_title_pattern; ?>
		</td>
       		<td>
			<?php echo $row->seo_title; ?>
		</td>
       		<td>
			<?php echo $row->seo_description; ?>
		</td>
       		<td>
			<?php echo $row->seo_keywords; ?>
		</td>
       		<td>
			<?php echo $row->seo_texttop; ?>
		</td>
       		<td>
			<?php echo $row->seo_textbottom; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
