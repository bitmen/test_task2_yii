<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('companyapartment_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->companyapartment_id),array('view','id'=>$data->companyapartment_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discount')); ?>:</b>
	<?php echo CHtml::encode($data->discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discount_description')); ?>:</b>
	<?php echo CHtml::encode($data->discount_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekday_from')); ?>:</b>
	<?php echo CHtml::encode($data->weekday_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekday_to')); ?>:</b>
	<?php echo CHtml::encode($data->weekday_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('holiday_from')); ?>:</b>
	<?php echo CHtml::encode($data->holiday_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('holiday_to')); ?>:</b>
	<?php echo CHtml::encode($data->holiday_to); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('event_time')); ?>:</b>
	<?php echo CHtml::encode($data->event_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info')); ?>:</b>
	<?php echo CHtml::encode($data->info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('announcement')); ?>:</b>
	<?php echo CHtml::encode($data->announcement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('has_logo')); ?>:</b>
	<?php echo CHtml::encode($data->has_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('public_state')); ?>:</b>
	<?php echo CHtml::encode($data->public_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_popular')); ?>:</b>
	<?php echo CHtml::encode($data->is_popular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decline_cause')); ?>:</b>
	<?php echo CHtml::encode($data->decline_cause); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advert_type')); ?>:</b>
	<?php echo CHtml::encode($data->advert_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paid_before')); ?>:</b>
	<?php echo CHtml::encode($data->paid_before); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment_count')); ?>:</b>
	<?php echo CHtml::encode($data->comment_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creation_time')); ?>:</b>
	<?php echo CHtml::encode($data->creation_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paid_link')); ?>:</b>
	<?php echo CHtml::encode($data->paid_link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utime')); ?>:</b>
	<?php echo CHtml::encode($data->utime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('auto_update')); ?>:</b>
	<?php echo CHtml::encode($data->auto_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_auto_update')); ?>:</b>
	<?php echo CHtml::encode($data->date_auto_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_auto_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_auto_update); ?>
	<br />

	*/ ?>

</div>