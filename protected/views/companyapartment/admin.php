<?php
$this->breadcrumbs=array(
	'Companyapartments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Companyapartment','url'=>array('index')),
	array('label'=>'Create Companyapartment','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('companyapartment-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Companyapartments</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'companyapartment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'companyapartment_id',
		'discount',
		'discount_description',
		'weekday_from',
		'weekday_to',
		'holiday_from',
		/*
		'holiday_to',
		'event_time',
		'title',
		'info',
		'announcement',
		'has_logo',
		'public_state',
		'status',
		'is_popular',
		'decline_cause',
		'advert_type',
		'paid_before',
		'comment_count',
		'creation_time',
		'update_time',
		'f_category_id',
		'f_user_id',
		'paid_link',
		'utime',
		'auto_update',
		'date_auto_update',
		'last_auto_update',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
