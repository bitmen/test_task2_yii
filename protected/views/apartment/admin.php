<?php
$this->breadcrumbs=array(
	'Apartments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Apartment','url'=>array('index')),
	array('label'=>'Create Apartment','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('apartment-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Apartments</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'apartment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'apartment_id',
		'_old_id',
		'cost',
		'appliances',
		'furniture',
		'storey',
		/*
		'storey_count',
		'contact',
		'square',
		'square_life',
		'f_company_id',
		'title',
		'address',
		'info',
		'phone',
		'email',
		'skype',
		'has_logo',
		'public_state',
		'status',
		'creation_time',
		'update_time',
		'decline_cause',
		'f_user_id',
		'f_category_id',
		'f_sub_category_id',
		'comment_count',
		'f_city_id',
		'f_district_id',
		'paid_before',
		'advert_type',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
