<?php
$this->breadcrumbs=array(
	'Apartments'=>array('index'),
	$model->title,
);
?>

<h1>View Apartment #<?php echo $model->apartment_id; ?></h1>
<hr />
<?php 
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		array('label'=>'Create', 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                array('label'=>'List', 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'), 'linkOptions'=>array()),
                array('label'=>'Update', 'icon'=>'icon-edit', 'url'=>Yii::app()->controller->createUrl('update',array('id'=>$model->apartment_id)), 'linkOptions'=>array()),
		//array('label'=>'Search', 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		array('label'=>'Print', 'icon'=>'icon-print', 'url'=>'javascript:void(0);return false', 'linkOptions'=>array('onclick'=>'printDiv();return false;')),

)));
$this->endWidget();
?>
<div class='printableArea'>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'apartment_id',
		'_old_id',
		'cost',
		'appliances',
		'furniture',
		'storey',
		'storey_count',
		'contact',
		'square',
		'square_life',
		'f_company_id',
		'title',
		'address',
		'info',
		'phone',
		'email',
		'skype',
		'has_logo',
		'public_state',
		'status',
		'creation_time',
		'update_time',
		'decline_cause',
		'f_user_id',
		'f_category_id',
		'f_sub_category_id',
		'comment_count',
		'f_city_id',
		'f_district_id',
		'paid_before',
		'advert_type',
	),
)); ?>
</div>
<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>
