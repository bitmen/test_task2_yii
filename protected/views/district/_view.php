<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('district_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->district_id),array('view','id'=>$data->district_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_city_id')); ?>:</b>
	<?php echo CHtml::encode($data->f_city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('_old_id')); ?>:</b>
	<?php echo CHtml::encode($data->_old_id); ?>
	<br />


</div>