<?php

/**
 * This is the model class for table "ur_apartment".
 *
 * The followings are the available columns in table 'ur_apartment':
 * @property string $apartment_id
 * @property string $_old_id
 * @property string $cost
 * @property integer $appliances
 * @property integer $furniture
 * @property integer $storey
 * @property integer $storey_count
 * @property string $contact
 * @property integer $square
 * @property integer $square_life
 * @property string $f_company_id
 * @property string $title
 * @property string $address
 * @property string $info
 * @property string $phone
 * @property string $email
 * @property string $skype
 * @property integer $has_logo
 * @property integer $public_state
 * @property integer $status
 * @property string $creation_time
 * @property string $update_time
 * @property string $decline_cause
 * @property string $f_user_id
 * @property integer $f_category_id
 * @property integer $f_sub_category_id
 * @property integer $comment_count
 * @property integer $f_city_id
 * @property integer $f_district_id
 * @property string $paid_before
 * @property integer $advert_type
 *
 * The followings are the available model relations:
 * @property UrCategory $fCategory
 * @property UrCity $fCity
 * @property UrDistrict $fDistrict
 * @property UrCounter $apartment
 * @property UrCounter $fCompany
 * @property UrSubCategory $fSubCategory
 * @property UrUser $fUser
 */
class Apartment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_apartment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('apartment_id, cost, appliances, furniture, storey, storey_count, title, address, email, f_user_id, f_category_id, f_sub_category_id, f_city_id, f_district_id', 'required'),
			array('appliances, furniture, storey, storey_count, square, square_life, has_logo, public_state, status, f_category_id, f_sub_category_id, comment_count, f_city_id, f_district_id, advert_type', 'numerical', 'integerOnly'=>true),
			array('apartment_id, _old_id, f_company_id, f_user_id', 'length', 'max'=>20),
			array('cost, contact, phone, email, skype', 'length', 'max'=>100),
			array('title, address', 'length', 'max'=>250),
			array('info, creation_time, update_time, decline_cause, paid_before', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('apartment_id, _old_id, cost, appliances, furniture, storey, storey_count, contact, square, square_life, f_company_id, title, address, info, phone, email, skype, has_logo, public_state, status, creation_time, update_time, decline_cause, f_user_id, f_category_id, f_sub_category_id, comment_count, f_city_id, f_district_id, paid_before, advert_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fCategory' => array(self::BELONGS_TO, 'UrCategory', 'f_category_id'),
			'fCity' => array(self::BELONGS_TO, 'UrCity', 'f_city_id'),
			'fDistrict' => array(self::BELONGS_TO, 'UrDistrict', 'f_district_id'),
			'apartment' => array(self::BELONGS_TO, 'UrCounter', 'apartment_id'),
			'fCompany' => array(self::BELONGS_TO, 'UrCounter', 'f_company_id'),
			'fSubCategory' => array(self::BELONGS_TO, 'UrSubCategory', 'f_sub_category_id'),
			'fUser' => array(self::BELONGS_TO, 'UrUser', 'f_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'apartment_id' => 'Apartment',
			'_old_id' => 'Old',
			'cost' => 'Cost',
			'appliances' => 'Appliances',
			'furniture' => 'Furniture',
			'storey' => 'Storey',
			'storey_count' => 'Storey Count',
			'contact' => 'Contact',
			'square' => 'Square',
			'square_life' => 'Square Life',
			'f_company_id' => 'F Company',
			'title' => 'Title',
			'address' => 'Address',
			'info' => 'Info',
			'phone' => 'Phone',
			'email' => 'Email',
			'skype' => 'Skype',
			'has_logo' => 'Has Logo',
			'public_state' => 'Public State',
			'status' => 'Status',
			'creation_time' => 'Creation Time',
			'update_time' => 'Update Time',
			'decline_cause' => 'Decline Cause',
			'f_user_id' => 'F User',
			'f_category_id' => 'F Category',
			'f_sub_category_id' => 'F Sub Category',
			'comment_count' => 'Comment Count',
			'f_city_id' => 'F City',
			'f_district_id' => 'F District',
			'paid_before' => 'Paid Before',
			'advert_type' => 'Advert Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('apartment_id',$this->apartment_id,true);
		$criteria->compare('_old_id',$this->_old_id,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('appliances',$this->appliances);
		$criteria->compare('furniture',$this->furniture);
		$criteria->compare('storey',$this->storey);
		$criteria->compare('storey_count',$this->storey_count);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('square',$this->square);
		$criteria->compare('square_life',$this->square_life);
		$criteria->compare('f_company_id',$this->f_company_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('has_logo',$this->has_logo);
		$criteria->compare('public_state',$this->public_state);
		$criteria->compare('status',$this->status);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('decline_cause',$this->decline_cause,true);
		$criteria->compare('f_user_id',$this->f_user_id,true);
		$criteria->compare('f_category_id',$this->f_category_id);
		$criteria->compare('f_sub_category_id',$this->f_sub_category_id);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('f_city_id',$this->f_city_id);
		$criteria->compare('f_district_id',$this->f_district_id);
		$criteria->compare('paid_before',$this->paid_before,true);
		$criteria->compare('advert_type',$this->advert_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Apartment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
