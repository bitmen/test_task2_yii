<?php

/**
 * This is the model class for table "ur_category".
 *
 * The followings are the available columns in table 'ur_category':
 * @property integer $category_id
 * @property string $title
 * @property integer $list_pos
 * @property integer $f_type_id
 * @property string $translit
 * @property integer $_old_id
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_texttop
 * @property string $seo_textbottom
 *
 * The followings are the available model relations:
 * @property UrApartment[] $urApartments
 * @property UrType $fType
 * @property UrCompanyapartment[] $urCompanyapartments
 * @property UrSubCategory[] $urSubCategories
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ur_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, f_type_id', 'required'),
			array('list_pos, f_type_id, _old_id', 'numerical', 'integerOnly'=>true),
			array('title, translit', 'length', 'max'=>100),
			array('seo_title, seo_keywords', 'length', 'max'=>250),
			array('seo_description, seo_texttop, seo_textbottom', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('category_id, title, list_pos, f_type_id, translit, _old_id, seo_title, seo_description, seo_keywords, seo_texttop, seo_textbottom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urApartments' => array(self::HAS_MANY, 'UrApartment', 'f_category_id'),
			'fType' => array(self::BELONGS_TO, 'UrType', 'f_type_id'),
			'urCompanyapartments' => array(self::HAS_MANY, 'UrCompanyapartment', 'f_category_id'),
			'urSubCategories' => array(self::HAS_MANY, 'UrSubCategory', 'f_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'category_id' => 'Category',
			'title' => 'Title',
			'list_pos' => 'List Pos',
			'f_type_id' => 'F Type',
			'translit' => 'Translit',
			'_old_id' => 'Old',
			'seo_title' => 'Seo Title',
			'seo_description' => 'Seo Description',
			'seo_keywords' => 'Seo Keywords',
			'seo_texttop' => 'Seo Texttop',
			'seo_textbottom' => 'Seo Textbottom',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('list_pos',$this->list_pos);
		$criteria->compare('f_type_id',$this->f_type_id);
		$criteria->compare('translit',$this->translit,true);
		$criteria->compare('_old_id',$this->_old_id);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_description',$this->seo_description,true);
		$criteria->compare('seo_keywords',$this->seo_keywords,true);
		$criteria->compare('seo_texttop',$this->seo_texttop,true);
		$criteria->compare('seo_textbottom',$this->seo_textbottom,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
